<?php
$target_dir = "uploads/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);


if (file_exists($target_file)) {
    echo "El archivo ya existe...";
    $uploadOk = 0;
}

//Allow just png files
if($imageFileType != "png") {
    echo "Por favor utiliza un archivo png";
    $uploadOk = 0;
}
if ($uploadOk == 0) {
    echo "Tu archivo no se subio, intenta nuevamente.";
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        echo "Se subio el archivo: ". basename( $_FILES["fileToUpload"]["name"]). " correctamente.";
        header("location:lab10.html?color=indigo");
    } else {
        echo "Hubo un error subiendo tu archivo, intenta nuevamente.";
    }
}
?>
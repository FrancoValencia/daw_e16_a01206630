<?php if(isset($_SESSION["error"])): ?>
    <h3>Hay errores en la forma</h3>
    <?php unset($_SESSION["error"]); ?>
<?php endif; ?>

<?php if(isset($_SESSION["name"])): ?>
    <h3>Bienvenido <?php echo  $_SESSION["name"] ?></h3>
    <?php include("_Generate.php"); ?>
<?php elseif(isset($_SESSION["name"]) && isset($_SESSION["calendar"])): ?>
	<h3>Tu calendario <?php echo  $_SESSION["name"] ?></h3>
    <?php include("generate.php"); ?>    
<?php else: ?>
    <h3>Ingresa para generar tu calendario</h3>
    <?php include("_Login.php"); ?>
<?php endif; ?>

// reu 2016 - Franco Valencia

// create, register and retrieve reu module
// 'reu.services' is found in services.js
// 'reu.controllers' is found in controllers.js
angular.module('reu', ['ionic','ionic.service.core', 'reu.controllers', 'reu.services', "firebase"])

// ionic 
.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

// Set application status
.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider

  // Login state
    .state('login', {
      url: '/login',
      templateUrl: 'templates/login.html',
      controller: 'LoginCtrl'
    })

  // setup an abstract state for the tabs directive
    .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })

  // Contacts state
  .state('tab.contacts', {
    url: '/contacts',
    views: {
      'tab-contacts': {
        templateUrl: 'templates/tab-contacts.html',
        controller: 'ContactsCtrl'
      }
    }
  })

  // Events state
  .state('tab.events', {
      url: '/events',
      views: {
        'tab-events': {
          templateUrl: 'templates/tab-events.html',
          controller: 'EventsCtrl'
        }
      }
    })

  // Event creation form state
  .state('event-create', {
      url: '/event-create',
      templateUrl: 'templates/event-create.html',
      controller: 'EventCreateCtrl'

    })


  .state('tab.event-detail', {
      url: '/whatson/:eventId',
      views: {
        'tab-whatson': {
          templateUrl: 'templates/event-detail.html',
          controller: 'EventDetailCtrl'
        }
      }
    })
    .state('tab.whatson', {
      url: '/whatson',
      views: {
        'tab-whatson': {
          templateUrl: 'templates/tab-whatson.html',
          controller: 'WhatsCtrl'
        }
      }
    })

  .state('tab.settings', {
    url: '/settings',
    views: {
      'tab-settings': {
        templateUrl: 'templates/tab-settings.html',
        controller: 'SettingsCtrl'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/login');

});

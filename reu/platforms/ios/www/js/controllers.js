// reu 2016 - Franco Valencia
// TODO - MUSTS

// Fix navigation broken links
// Owner can delete the event
// Make more smooth the user is attending methods
// Event Create Form validation


// TODO
// Event Create change to datepicker
// Add image upload on event creating
// Add location compatible with maps/google maps
// Add friend management
// Add share link for mobile/whatsapp
// Add tutorial
// Add function to edit an event
// Add event privacy
// Ask for update if using older version
// Autodestruct events after x time

// IDEAS
// Bring stuff list

// register reu contrillers module
angular.module('reu.controllers', [])

// MARK -- LOGIN CONTROLLER
// What it does: Gets ref data from firebase, authethicates with OAuth and writes new users on reu.firebase.com/users
// Where it goes: To tab-whatson on successfull login
.controller('LoginCtrl', function($scope, $firebaseAuth, fireBaseData, $ionicPopup, $state) {
  var ref = fireBaseData.ref();
  $scope.login = function(serviceName){
    ref.authWithOAuthPopup(serviceName, function(error, authData) {
      if (error) {
        console.log("Login Failed:", error);
        var alertPopup = $ionicPopup.alert({
        title: 'Login failed!',
        template: 'Please check your credentials!'
      });
      } else {
        $state.go('tab.whatson');
        console.log("Authenticated successfully with payload:", authData);
        ref.child("users/" + authData.uid).set(authData);
      }
    });
}
})

// MARK - CONTACTS CONTROLLER
// What it does: Gets user data as array from firebase
.controller('ContactsCtrl', function($scope, fireBaseData) {
  $scope.users = fireBaseData.refUsers();

})


// MARK -- WHATSON
// What it does: Gets all the events as array for display on the tab-whatson view
.controller('WhatsCtrl', function($scope, fireBaseData) {
 $scope.events = fireBaseData.refEvents();
})

// MARK -- EVENTS CONTROLLER
// What it does: Gets all the events as array for display
// Where it goes: Allouds
.controller('EventsCtrl', function($scope, fireBaseData, $state){
    $scope.events = fireBaseData.refEvents();
    $scope.filters = {};
    $scope.user = fireBaseData.ref().getAuth();

    // Code to run each time view is entered
    $scope.$on('$ionicView.enter', function() {
      $scope.user = fireBaseData.ref().getAuth();
    });

    $scope.newEvent = function (){
      $state.go('event-create');
    }


})

// MARK -- EVENTDETAIL
// What it does: Gets reu.firebase.com/events As an object, to show it 
// Where it goes: Tab About - Tab Chat - Tab-More

.controller('EventDetailCtrl', function($scope, $stateParams, $firebaseArray, fireBaseData, $state) {
  var eventId = $stateParams.eventId;
  var ref = fireBaseData.ref();

  $scope.user = fireBaseData.ref().getAuth();
  $scope.event = fireBaseData.refEvent(eventId);
  $scope.selection;
  $scope.viewerIsOwner;

  // ---- EVENTDATAIL: GENERAL ----  
  // On load, see if viewer is already attending to the event
  $scope.event.$loaded().then(function(array){
    $scope.selection = '';
    $scope.checkIfViewerIsAttending(array);
    $scope.checkIfViewerIsOwner();
  });

  // Watch if there are any changes to the database, if checks if the user is already attending to the event
  // WARNING: Must check if method lives outside EventDetail view.
  var unwatch = $scope.event.$watch(function() {
    var array = $scope.event;
    $scope.checkIfViewerIsAttending(array);
    $scope.checkIfViewerIsOwner();
  });

  // Checks the current selection, updates $scope.selection
  $scope.selectTab = function(tab){
    $scope.selection = tab;
  }
  
  //Check if the viewer is attending, receives attending data as array.
  $scope.checkIfViewerIsAttending = function(array){
      for(i = 0; i < array.attendees.length; i++ ){
      if(array.attendees[i] == $scope.user.uid){
        $scope.selection = 'about';
      }else{
        $scope.selection = '';
      }
    }
  }
  // Check if the viwer is the owner from the event
  $scope.checkIfViewerIsOwner = function(){
    var array = $scope.event;
    if(array.owner == $scope.user.uid ){
      console.log('You are the event owner!');
      $scope.viewerIsOwner= true;
    }else{
      console.log("You are not the event owner!");
      $scope.vieweisOwner = false;
    }
    console.log(array.owner);
    console.log($scope.user.uid);
    

  }

  // ---- EVENT DETAIL: DEFAULT TAB ----

  // Add the viwer to the event passing his user.uid as parameter
  $scope.addUserToEvent = function(){
    $scope.event.attendees.push($scope.user.uid);

    $scope.event.$save().then(function() {
        console.log('Attendee saved!');
      }).catch(function(error) {
        console.log('Error!');
      });
  };
  // ---- EVENT DETAIL: ABOUT TAB ----

  // ---- EVENT DETAIL: CHAT TAB ----

  // ---- EVENT DETAIL: MORE TAB ----


  // Remove the viewer from the event pasing his user.uid as parameter
  // WARNING: Must check its working for more than n=2 attendees
  $scope.removeUserFromEvent = function(){
    $scope.event.attendees.pop($scope.user.uid);
    $scope.event.$save().then(function() {
        //$state.go('tab.whatson');
      }).catch(function(error) {
        console.log('Error removing the attendee!');
      });
  };

})



// MARK - EVENT CREATE
// What it does: Gets the events ref, creates new events though a form and pushes the changes
// Where it goes: Goes to my events-hosting tab
.controller('EventCreateCtrl', function($scope, fireBaseData, $state) {
  $scope.events = fireBaseData.refEvents();
  $scope.user = fireBaseData.ref().getAuth();
  

  // Code to run each time view is entered
  $scope.$on('$ionicView.enter', function() {
    $scope.createEventForm = {};
    $scope.user = fireBaseData.ref().getAuth();
  });

  // ---- EVENT CREATE: CREATE EVENT FORM ----

  //Creates the event on Firebase, passes the owner_id and owner_picture recovered from authdata
  $scope.createEvent = function() {

    //WARNING: I should pass only the owner id, so that all controllers
    //         that need to display the user name and picture can get 
    //         it directly by comparing the user.uid directly
    //         from the firebase users ref.
    $scope.events.$add({
      owner: $scope.user.uid,
      owner_name: $scope.getDisplayName(),
      owner_img: $scope.getPicture(),
      title: $scope.createEventForm.title,
      date: $scope.createEventForm.date,
      //Change date to DD_MM_YYYY form... Maybe date picker?
      image: $scope.createEventForm.image,
      about: $scope.createEventForm.about,
      location: $scope.createEventForm.location,
      //add privacy
      //add invited users 
      attendees: [$scope.user.uid]
    });
    $state.go('tab.whatson');
  };

  // ---- EVENT CREATE: GET METHODS FROM AUTH ----
  $scope.getDisplayName = function(){
    var rtnDisplayName = '';
    if ($scope.getProvider()  == "google"){
      rtnDisplayName = $scope.user.google.displayName;
    }else if ($scope.getProvider()  == "facebook"){
      rtnDisplayName = $scope.user.facebook.displayName;
    }
    return rtnDisplayName;
  }
  $scope.getPicture = function(){
    var rtnPicture = '';
    if ($scope.getProvider() =="google"){
      rtnPicture = $scope.user.google.cachedUserProfile.picture;
    }else if ($scope.getProvider()  == "facebook"){
      rtnPicture = $scope.user.facebook.cachedUserProfile.picture.data.url;
    }
    return rtnPicture;
  }
  $scope.getProvider = function(){
    var  rtnProvider = '';
    if ($scope.user.provider =="google"){
      rtnProvider = 'google';
    }else if ($scope.user.provider == "facebook"){
      rtnProvider = 'facebook';
    }
    return rtnProvider;
  }
  
})

// MARK - SETTINGS
// What it does: Displays user information
.controller('SettingsCtrl', function($scope, fireBaseData, $state) {
  $scope.user = fireBaseData.ref().getAuth();

  // Code to run each time view is entered
  $scope.$on('$ionicView.enter', function() {
    $scope.user = fireBaseData.ref().getAuth();
  });
  
  $scope.version = 'Alpha 0.1.3.21'

  // Log out the user
  $scope.logout = function () {
    fireBaseData.ref().unauth();
    $state.go('login');
  };

  // ---- SETTINGS: GET METHODS FROM AUTH ----
  $scope.getDisplayName = function(){
    var rtnDisplayName = '';
    if ($scope.getProvider()  == "google"){
      rtnDisplayName = $scope.user.google.displayName;
    }else if ($scope.getProvider()  == "facebook"){
      rtnDisplayName = $scope.user.facebook.displayName;
    }
    return rtnDisplayName;
  }
  $scope.getPicture = function(){
    var rtnPicture = '';
    if ($scope.getProvider() =="google"){
      rtnPicture = $scope.user.google.cachedUserProfile.picture;
    }else if ($scope.getProvider()  == "facebook"){
      rtnPicture = $scope.user.facebook.cachedUserProfile.picture.data.url;
    }
    return rtnPicture;
  }
  $scope.getProvider = function(){
    var  rtnProvider = '';
    if ($scope.user.provider =="google"){
      rtnProvider = 'google';
    }else if ($scope.user.provider == "facebook"){
      rtnProvider = 'facebook';
    }
    return rtnProvider;
  }

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

})


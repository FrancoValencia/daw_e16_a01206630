// reu 2016 - Franco Valencia

// register reu services module
angular.module('reu.services', [])

// MARK -- FIREBASEDATA
// What it does: Gets ref to reu's firebase
// What it returns: Events, Event, Users, Ref
.factory('fireBaseData', function($firebaseArray, $firebaseObject) {
    var ref = new Firebase("https://reu.firebaseio.com/");

    return {
        ref: function() {
            return ref;
        },
        refEvents: function() {
            return refEvents = $firebaseArray(ref.child('events'));
        },
        refEvent: function(eventId){
          return refEvent = $firebaseObject(ref.child('events').child(eventId));
        },
        refUsers: function() {
            return refUsers =  $firebaseArray(ref.child('users'))
        },
        refUser: function(userId){
            return refUser = $firebaseObject(ref.child('users').child(userId));
        },
        refUserDisplayName: function(userId, userProvider){
            //return refUserDisplayName = $firebaseObject(ref.child('users').child(userId).child(userProvider));
        }
    }
})



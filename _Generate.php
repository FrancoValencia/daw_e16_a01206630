<div class="card-panel grey lighten-4">
  <div class="row">
    <form class="col s12" action="generate.php" method="POST">
      <div class="row">
        <div class="input-field col s12">
          <input name="uMonth" type="text">
          <label for="uMonth">Mes</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <input name="uDay" type="text">
          <label for="uDay">Día de hoy</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <input name="uYear" type="text">
          <label for="uYear">Año</label>
        </div>
      </div>
      <div class="row">
        <div class="col s6">
          <input class="btn btn-large waves-effect waves-light indigo black-text" type="submit" name="action" value="Ir"></input>
        </div>
      </div>
    </form>
  </div>
</div>
<br><br>
<div class="row">
  <form method ="get" action="logout.php">
    <div class="col6">
      <input class="btn btn-large waves-effect waves-light indigo black-text" type="submit" value="Salir"></input>
    </div>
  </form>
</div>
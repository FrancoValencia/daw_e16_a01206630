<?php
$numbers = array (1,3,2,13,7,6,5,15,9,10,11);

function average($array){
  return ((array_sum($array))/count($array));
}
function median ($array){
  $count = count($array);
  $valormedio = floor(($count-1)/2);
  if($count %2){
  return $array[$valormedio];
  }else {
  return ((($array[$valormedio])+($array[$valormedio+1])));
  }
}
function printArray($array){
  for($i = 0; $i < count($array); $i++) {
  echo $array[$i];
  echo ", ";
  }
}
function printSorted($array){
  echo "<br>";
  echo "Sorted: ";
  sort($array);
  printArray($array);
}
function printResorted($array){
  echo "<br>";
  echo "Reverse sorted: ";
  rsort($array);
  printArray($array);
}
function powerTable($n){
  $maxcols = 3;

  echo "<table>";
  echo "<tr>";
    echo "<td>Number</td><td>^2</td><td>^3</td>";
    echo "</tr>";
    
    for ($i = 1; $i < $n +1; $i++){
    echo "<tr>";
      $isquared = pow($i, 2);
      $icubed = pow($i, 3);
      echo "<td>$i</td><td>$isquared</td><td>$icubed</td>";
      }
    echo "</tr>";
  echo "</table>";
}
function bmi($height, $weight){
  echo "<br>";
  $bmi = $weight / ($height * $height);
  echo "BMI: " . $bmi;
}
?>
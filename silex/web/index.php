<?php

require_once __DIR__.'/../vendor/autoload.php';

$app = new Silex\Application();

$app->get('/hello/{name}', function ($name) {
    return 'Hello ' .$name. 'I hope you get done with DAW before he gets done with you first!';
});

$app->run();

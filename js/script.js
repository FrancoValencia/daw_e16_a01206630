//MARK - Primer Script

function script1(){


	//Solicita el numero entero.
	var tableLength = prompt("Ingresa un numero entero:");

	//Valida que sea un numero entero. Si es un numero procede a crear la tabla.
	if (tableLength % 1 == 0){

		//Crea la tabla con el largo definido por el usuario y el ancho de tres.
		var tableContent = fillTable(tableLength,3);

		//Crea cada fila y columna y los llena con los items que regresa la funcion fillTable.
		tableContent.forEach(function(items) {
  		var row = document.createElement("tr");
  		items.forEach(function(item) {
    		var cell = document.createElement("td");
    		cell.textContent = item;
    		row.appendChild(cell);
  		});
  	tableForScript1.appendChild(row);
});
		
	//En caso de que no sea un numero entero.
	}else {
		alert("Debes ingresar un numero entero.")
	}

}


//Llena la tabla con los valores deseados
function fillTable(width, height){
    var result= [];
    //Crea un arreglo dentro de otro arreglo y lo cicla.
    for (var i = 0 ; i < height; i++) {
        result[i] = [];
        for (var j = 0; j < width; j++) {
        	//Rellena cada celda con la formula que deberia de llevar que es (j+1)ˆ(i+1)
            result[i][j] = (Math.pow(j+1,i+1));
        }
    }
    return result;
}

//MARK - Segundo Script

function script2(){
	//Genera dos numeros aleatorios entre 0 y 100.
	var x = Math.floor(Math.random() * 99) + 1; var y = Math.floor(Math.random() * 99) + 1;
	//Crea las variables para medir el tiempo de respuesta.
	var solvedTime; var askedTime; var reactionTime; 
	askedTime=Date.now();


	//Prompt preguntado el resultado.
	var result = prompt("¿Cual es el resultado de: " + x + " + " + y + "?", "Valor por defecto");

	//Calcula el tiempo que tomo en resolverlo

	solvedTime = Date.now();
	reactionTime = (solvedTime - askedTime)/1000; 

	//Verifica que el resultado sea correcto.
	if (result == x+y){
		alert("Correcto! Tu tiempo de reacción es de: " + reactionTime + " segundos.");
	}else{
		alert("Resultado incorrecto, intenta nuevamente");
	}



}

//MARK - Tercer Script

function script3(){
	//Declarar variables contadores
	var counterNeg = 0; var counterZeros = 0; var counterInt = 0;

	//Solicita al usuario un arreglo
	var arr = prompt("Inserta numeros separados por comas.").split(",");


	//Checa cada elemento del arreglo para ver si es un numero negativo o un cero.
	for (i=0;i<arr.length; i++){

		if (arr[i] < 0){
			counterNeg++;

		}else if(arr[i] == 0){
			counterZeros++;
		}

		else{
			counterInt++;
		}
	}

	alert("Cantidad de negativos: " + counterNeg  + "\nCantidad de ceros: " + counterZeros + "\nCantidad de enteros: " + counterInt);
}

//MARK - Cuarto Script

function script4(){

	//Quiero que el usuario entre el arreglo.
	var arr = [];
	var arrAverage = [];
	var arrNumber = prompt("Ingrese el numero de arreglos:");

	for (i=0;i<arrNumber;i++){
		arr.push(prompt("Inserta numeros separados por comas.").split(","));
	}


	//Hago un nuevo ciclo para no "hacer trampa".
	for (i=0;i<arrNumber;i++){
		var sum = 0;
		for(j=0;j<arr[i].length;j++){
			sum += parseInt(arr[i][j], 10);
		}
		arrAverage[i] = sum / arr[i].length;
	}

	alert(arrAverage);
}

//MARK - Quinto Script

function script5(){

	//Solicita al usuario el numero
	var magicNumer = prompt("Ingrese un numero con al menos dos digitos:");
	var remunCigam = magicNumer.toString();
	var response = [];

	//Valida que sea un numero entero de dos digitos.
	if (magicNumer % 1 == 0 && magicNumer > 9){

		//Empuja los numeros al array response en orden inverso.
		for(i=remunCigam.length; i>-1; i--){
			response.push(remunCigam.charAt(i));
		}

		//Junta el array y elimina las comas de separación.
		response = response.join("");
		alert(response);

	}else{
		alert("El numero debe terner al menos dos digitos y ser entero.");
	}
}

//MARK - Sexto Script

function script6(){
	alert("Hello World")
}

